package com.example;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Assertions;

/**
 * Unit test for simple App.
 */
@ExtendWith(MockitoExtension.class)
class ContactServiceMockTest {

    @Mock
    private IContactDao contactDao;

    @InjectMocks
    private ContactService contactService = new ContactService();

    @Test
    void shouldFailDuplicate() {
        Mockito.when(contactDao.isContactExist("thierry")).thenReturn(true);

        Assertions.assertThrows(ContactDuplicateException.class, 
        () -> contactService.creerContact("thierry"));
    }

    @Test
    void shouldPass() {
        Mockito.when(contactDao.isContactExist("thierry")).thenReturn(false);
        contactService.creerContact("thierry");
    }


    @Test
    void shouldPassDuplicate() {
        // Enregistrement du comportement du mock
        Mockito.when(contactDao.isContactExist("thierry")).thenReturn(false);

        // Appel du service
        contactService.creerContact("ThierrY");
    
        // Verification des appels effectués
        Mockito.verify(contactDao).addContact("thierry");
    }

}
